<?php

namespace BeiWork\Logger\Methods;

interface MethodInterfaceFactory
{
    public function writeLog($level, $message);
}