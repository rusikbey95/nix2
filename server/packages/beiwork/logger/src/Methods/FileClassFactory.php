<?php

namespace BeiWork\Logger\Methods;

use BeiWork\Logger\Singleton;

class FileClassFactory extends Singleton implements MethodInterfaceFactory
{
    private $handle;
    protected function __construct()
    {
        $file = '/storage/logs/' . date('Y-m-d') . '.txt';
        $this->handle = fopen($_SERVER['DOCUMENT_ROOT'] . 'MyLog.txt', 'a+');
    }

    public static function log($message)
    {
    }

    public function writeLog($level, $message)
    {
        $date = date("Y-m-d G:i:s");
        $str = $level . ' | ' . $date . ' ' . print_r($message, true). "\r\n";
        fwrite($this->handle, $str);
    }
}