<?php

namespace BeiWork\Framework\Views;

/**
 * Class View
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 */
class View
{
    /**
     * @var array
     */
    public array $route = [];
    /**
     * @var string
     */
    public string $view;

    /**
     * @param array $route
     * @param string $view
     */
    public function __construct(array $route, string $view = '')
    {
        $this->route = $route;
        $this->view = $view;
    }

    /**
     * @param $data
     * @return void
     */
    public function render($data): void
    {
        if (is_array($data)) {
            extract($data);
        }
        
        $fileView = ROOT . '/app/Views/' . $this->view . '.php';
        ob_start();
        if (is_file($fileView)) {
            require $fileView;
        } else {
            echo '<h1>File ' . $fileView . ' not found!</h1>';
        }
        $content = ob_get_clean();

        $fileLayout = ROOT . '/app/Views/layouts/default.php';
        if (is_file($fileLayout)) {
            require $fileLayout;
        } else {
            echo '<h1>Layout file not found!</h1>';
        }
    }
}