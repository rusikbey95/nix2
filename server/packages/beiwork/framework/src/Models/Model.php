<?php

namespace BeiWork\Framework\Models;

use BeiWork\Framework\DB;
use JetBrains\PhpStorm\NoReturn;

/**
 * Class Model
 *
 * @author Ruslan Bei <rusikbey95@gmail.com>
 * @package BeiWork\Framework\Models
 */
abstract class Model
{
    /**
     * @var DB
     */
    protected $pdo;

    /**
     * @var string
     */
    protected string $table;

    /**
     * @var bool
     */
    protected bool $created_at = false;

    /**
     * @var bool
     */
    protected bool $update_at = false;

    /**
     * @var array
     */
    public array $user = [];

    public function __construct()
    {
        $this->pdo = DB::instance();
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $sql = "SELECT * FROM $this->table";
        return $this->pdo->query($sql);
    }

    /**
     * @param $search
     * @param $operator
     * @param $id
     * @return array
     */
    public function where($search, $operator, $id): array
    {
        $sql = "SELECT * FROM $this->table WHERE $search $operator '$id'";

        return call_user_func_array('array_merge', $this->pdo->query($sql));
    }

    /**
     * @param $search
     * @param $operator
     * @param $id
     * @param $column
     * @return array
     */
    public function whereOne($search, $operator, $id, $column): array
    {
        $sql = "SELECT $column FROM $this->table WHERE $search $operator '$id'";
        return call_user_func_array('array_merge', $this->pdo->query($sql));
    }

    /**
     * @param $array
     * @return array
     */
    public function insert($array): array
    {
        $array = array_map(function ($val) {
            return htmlspecialchars($val);
        }, $array);

        if ($this->created_at && $this->update_at) {
            date_default_timezone_set('Europe/Kiev');
            $arr = [
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $array = array_merge($array, $arr);
        }

        $keyString = implode(',', array_keys($array));
        $valuesString = implode('\',\'', array_values($array));

        $sql = "INSERT INTO $this->table ($keyString) VALUES ('$valuesString')";
//        dd($sql);
        return $this->pdo->query($sql);
    }

    /**
     * @param $search
     * @param $operator
     * @param $id
     * @return array
     */
    public function delete($search, $operator, $id): array
    {
        $sql = "DELETE FROM $this->table WHERE $search $operator '$id'";
        return $this->pdo->query($sql);
    }

    /**
     * @param $array
     * @return array
    */
    public function update($array): array
    {
        $array = array_map(function ($val) {
           return htmlspecialchars($val);
        }, $array);

        if ($this->update_at) {
            date_default_timezone_set('Europe/Kiev');
            $arr = [
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $array = array_merge($array, $arr);
        }

        $user_id = $array['id'];
        foreach ($array as $key => $value) {
            $arrayRes[] = $key . ' = ' . "'$value'";
        }

        $valuesString = implode(', ', array_values($arrayRes));

        $sql = "UPDATE $this->table SET $valuesString WHERE id  = $user_id";

        return $this->pdo->query($sql);
    }


    //sort
}