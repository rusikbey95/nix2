<?php

namespace BeiWork\Framework\Controllers;

use BeiWork\Framework\Views\View;

/**
 * Class Controller
 *
 * @author Ruslan Bei <rusikbey95@gmail.com>
 * @package BeiWork\Framework\Controllers
 */
abstract class Controller
{
    /**
     * @var string
     */
    public string $view;

    /**
     * @var array
     */
    public array $route = [];
    public array $data = [];

    /**
     * @param $route
     */
    public function __construct($route)
    {
        $this->route = $route;
    }

    /**
     * @param string $view
     * @param array $data
     * @return bool
     */
    public function view(string $view, array $data = []): bool
    {
        $viewObject = new View($this->route, $view);
        if (empty($this->data)) {
            $data = array_merge($data, $this->data);
        }

        $viewObject->render($data);
        return true;
    }

    /**
     * @param $data
     */
    public function set($data)
    {
        $this->data = $data;
    }
}