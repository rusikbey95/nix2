<?php
/**
 * @param $array
 */
function dump($array): void
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

/**
 * @param $array
 */
function dd($array): void
{
    dump($array);
    die();
}