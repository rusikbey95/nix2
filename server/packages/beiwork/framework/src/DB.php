<?php

namespace BeiWork\Framework;

use PDO;

/**
 * Class DB
 *
 * @author Ruslan Bei <rusikbey95@gmail.com>
 * @package BeiWork\Framework
 */
class DB
{
    /**
     * @var object|PDO
     */
    protected object $pdo;
    protected static $instance;


    protected function __construct()
    {
        $db = require ROOT . '/config/database.php';

        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ];

        $this->pdo = new PDO($db['dns'], $db['username'], $db['password'], $options);
    }

    /**
     * @return DB|object
     */
    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * @param $sql
     * @param array $param
     * @return array
     */
    public function query($sql, array $param = []): array
    {
        $PDOStatement = $this->pdo->prepare($sql);

        $result = $PDOStatement->execute($param);

        if ($result !== false) {
            return $PDOStatement->fetchAll();
        }

        echo 'По данному запросу ничего не найдено';
        return [];
    }
}