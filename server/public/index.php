<?php

use BeiWork\Framework\Route;
use Symfony\Component\Dotenv\Dotenv;

require_once '../vendor/autoload.php';
require_once '../packages/beiwork/framework/src/function.php';

session_start();

define('URL', trim($_SERVER['REQUEST_URI'], ''));
define('ROOT', dirname(__DIR__));

$dotenv = new Dotenv();
$dotenv->load(ROOT . '/.env');

require_once ROOT . '/routes/web.php';

Route::dispatch(URL);