<?php

namespace App\Models;

use BeiWork\Framework\Models\Model;

/**
 * Class ArticleModel
 *
 * @author Ruslan Bei <rusikbey95@gmail.com>
 * @package App\Models
 */
class ArticleModel extends Model
{
    /**
     * @var string
     */
    protected string $table = 'articles';
    /**
     * @var bool
     */
    protected bool $created_at = true;
    /**
     * @var bool
     */
    protected bool $update_at = true;

    /**
     * @return array
     */
    public static function getAll(): array
    {
        $articleModel = new self;
        return $articleModel->findAll();
    }

    /**
     * @return array
     */
    public static function getArticle(): array
    {
        $articleModel = new self;
        return $articleModel->where('id', '=', 2);
    }

    /**
     * @param $array
     * @return void
     */
    public static function created($array): void
    {
        $ArticleModel = new self;
        $ArticleModel->insert($array);
    }
}