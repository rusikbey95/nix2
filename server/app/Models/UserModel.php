<?php

namespace App\Models;

use BeiWork\Framework\Models\Model;

/**
 * Class UserModel
 *
 * @author Ruslan Bei <rusikbey95@gmail.com>
 * @package App\Models
 */
class UserModel extends Model
{
    /**
     * @var string
     */
    protected string $table = 'users';
    /**
     * @var bool
     */
    protected bool $created_at = true;
    /**
     * @var bool
     */
    protected bool $update_at = true;

    /**
     * @param $str
     * @return string
     */
    public function hashPassword($str): string
    {
        if (isset($str)) {
            $str = password_hash($str, PASSWORD_DEFAULT);
        }
        return $str;
    }

    /**
     * @param $array
     * @return void
     */
    public static function created($array): void
    {
        $userModel = new self;
        $userEmail = $userModel->whereOne('email', '=', $array['email'], 'email');
        if (!empty($userEmail)) {
            echo "Sorry, this address already exists, please <a href='/register'>try again</a>";
            die();
        }
        $array['password'] = $userModel->hashPassword($array['password']);

        $userModel->insert($array);
        $userID = $userModel->whereOne('email', '=', $array['email'], 'id');
        if (!empty($userID)) {
            $_SESSION['id'] = $userID['id'];
        }
    }

    /**
     * @param $array
     * @return void
     */
    public static function authorized($array): void
    {
        $userModel = new self;
        if(isset($array)) {
            $email = htmlspecialchars($array['email']);
            $password = htmlspecialchars($array['password']);
            $dbId = '';
            $dbEmail = '';
            $dbPassword = '';

            $dbArray = $userModel->where('email', '=', $array['email']);
            if (!empty($dbArray)) {
                $dbId = $dbArray['id'];
                $dbEmail = $dbArray['email'];
                $dbPassword = $dbArray['password'];
            }

            $userEmail = ($dbEmail == $email);
            if (!$userEmail) {
                echo "This user does not exist, please <a href='/register'>register</a>";
                die();
            }
            if (!empty($userEmail) && password_verify($password, $dbPassword)) {
                $_SESSION['id'] = $dbId;
            } else {
                echo "Email or password is incorrect, please <a href='/login'>try again</a>";
                die();
            }
        }
    }

    /**
     * @return array
     */
    public static function getUser(): array
    {
        $userModel = new self;
        if (!empty($_SESSION['id'])) {
            return $userModel->where('id', '=', $_SESSION['id']);
        }
        return [];
    }

    /**
     * @param $id
     * @return array
     */
    public static function getOne($id): array
    {
        $userModel = new self;
        return $userModel->where('id', '=', $id);
    }

    /**
     * @param $id
     * @return array
     */
    public static function deleted($id): array
    {
        $userModel = new self;
        return $userModel->delete('id', '=', $_SESSION['id']);
    }

    /**
     * @param $array
     * @return void
     */
    public static function updated($array): void
    {
        $userModel = new self;
        $array['password'] = $userModel->hashPassword($array['password']);
        $userModel->update($array);
    }
}