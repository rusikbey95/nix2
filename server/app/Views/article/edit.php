<?php
//    dump($user);
?>
<main class="form-container w-100 m-auto">
    <form action="/list/update" method="post">
        <h1 class="h3 mb-3 text-center">Create article</h1>
        <div class="mb-2 hidden">
            <input type="text" class="form-control" id="user_id" name="users_id" value="<?= $user['id']; ?>" hidden>
        </div>
        <div class="mb-2">
            <label for="article_title">Title</label>
            <input type="text" class="form-control" id="article_title" name=title" value="<?= $article['title']; ?>">
        </div>
        <div class="mb-2">
            <label for="article_description">Title</label>
            <input type="text" class="form-control" id="article_description" name="description" value="<?= $article['description']; ?>">
        </div>

        <button class="w-100 btn btn-lg btn-primary" type="submit">Create</button>
    </form>
</main>
