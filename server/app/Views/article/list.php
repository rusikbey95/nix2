<?php
//dump($allUserArticles);
?>

<section class="article mb-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center mb-3">Articles</h2>
                <?php foreach ($allUserArticles as $article): ?>
                    <div class="row article-item">
                        <div class="col-md-12">
                            <a href="/list?<?php echo $article['id'] ?>"><?php echo $article['title'] ?></a>
                            <p><?php echo $article['description'] ?></p>
                        </div>
                    </div>
                <?php endforeach;?>
                <div class="mb-2">
                    <a href="/list/add" class="btn btn-primary">Create new article</a>
                </div>
            </div>
        </div>
    </div>
</section>
