<main class="form-container w-100 m-auto">
    <form action="/user/authorization" method="post">
        <h1 class="h3 mb-3 text-center">Please login</h1>
        <div class="mb-2">
            <label for="user_email">Email</label>
            <input type="email" class="form-control" id="user_email" name="email" required>
        </div>
        <div class="mb-2">
            <label for="user_password">Password</label>
            <input type="password" class="form-control" id="user_password" name="password" required>
        </div>
        <button class="w-100 btn btn-lg btn-primary" type="submit">login</button>
    </form>
</main>