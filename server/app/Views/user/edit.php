<?php
//    dump($user);
?>
<main class="form-container w-100 m-auto">
    <form action="/user/update" method="post">
        <h1 class="h3 mb-3 text-center">Profile edit</h1>
        <div class="mb-2 hidden">
            <input type="text" class="form-control" id="user_id" name="id" value="<?= $user['id']; ?>" hidden>
        </div>
        <div class="mb-2">
            <label for="user_first_name">First name</label>
            <input type="text" class="form-control" id="user_first_name" name="first_name" value="<?= $user['first_name']; ?>">
        </div>
        <div class="mb-2">
            <label for="user_last_name">Last name</label>
            <input type="text" class="form-control" id="user_last_name" name="last_name" value="<?= $user['last_name']; ?>">
        </div>
        <div class="mb-2">
            <span class="d-block">Gender</span>
            <div class="form-check">
                <input class="form-check-input" type="radio" id="gender_not_known" name="gender_id" value="0" required>
                <label class="form-check-label" for="gender_not_known">Not known</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" id="gender_male" name="gender_id" value="1" required>
                <label class="form-check-label" for="gender_male">Male</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" id="gender_female" name="gender_id" value="2" required>
                <label class="form-check-label" for="gender_female">Female</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" id="gender_not_applicable" name="gender_id" value="9" required>
                <label class="form-check-label" for="gender_not_applicable">Not applicable</label>
            </div>
        </div>
        <div class="mb-2">
            <label for="user_email">Email</label>
            <input type="email" class="form-control" id="user_email" name="email" value="<?= $user['email']; ?>">
        </div>
        <div class="mb-2">
            <label for="user_password">Password</label>
            <input type="password" class="form-control" id="user_password" name="password" value="<?= $user['password']; ?>">
        </div>
        <button class="w-100 btn btn-lg btn-primary" type="submit">Update</button>
    </form>
</main>

<script>
    // $('form').on('submit', function (e) {
    //     e.preventDefault();
    //     let data = $(this);
    //
    //     $.ajax({
    //         url: $('form').attr('action'),
    //         method: $('form').attr('method'),
    //         dataType: 'json',
    //         data: $(this).serialize(),
    //         success: function(data) {
    //             console.log(data);
    //             // redirect -> user page
    //         }
    //     })
    //
    //     console.log(12312313);
    // });
</script>
