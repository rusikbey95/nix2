<main class="form-container w-100 m-auto">
    <form action="/user/create" method="post">
        <h1 class="h3 mb-3 text-center">Please register</h1>
        <div class="mb-2">
            <label for="user_first_name">First name</label>
            <input type="text" class="form-control" id="user_first_name" name="first_name" required>
        </div>
        <div class="mb-2">
            <label for="user_last_name">Last name</label>
            <input type="text" class="form-control" id="user_last_name" name="last_name" required>
        </div>
        <div class="mb-2">
            <label for="user_photo">Photo</label>
            <input type="file" class="form-control" id="user_photo" name="image">
        </div>
        <div class="mb-2">
            <label for="user_gender">Gender</label>
            <select id="user_gender" class="form-select" name="gender_id" required>
                <option value="0">Not known</option>
                <option value="1">Male</option>
                <option value="2">Female</option>
                <option value="9">Not applicable</option>
            </select>
        </div>
        <div class="mb-2">
            <label for="user_email">Email</label>
            <input type="email" class="form-control" id="user_email" name="email" required>
        </div>
        <div class="mb-2">
            <label for="user_password">Password</label>
            <input type="password" class="form-control" id="user_password" name="password" required>
        </div>
        <button class="w-100 btn btn-lg btn-primary" type="submit">Register</button>
    </form>
</main>

<?php //dump($_POST); ?>

<script>
    // $('form').on('submit', function (e) {
    //     e.preventDefault();
    //     let data = $(this);
    //
    //     $.ajax({
    //         url: $('form').attr('action'),
    //         method: $('form').attr('method'),
    //         dataType: 'json',
    //         data: $(this).serialize(),
    //         success: function(data) {
    //             console.log(data);
    //             // redirect -> user page
    //         }
    //     })
    //
    //     console.log(12312313);
    // });
</script>
