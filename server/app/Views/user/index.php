<main class="form-container w-100 m-auto">
    <h1 class="h3 mb-3 text-center">Profile</h1>
    <div class="mb-2">
        <b>First name:</b>
        <span><?= $user['first_name']; ?></span>
    </div>
    <div class="mb-2">
        <b>Last name:</b>
        <span><?= $user['last_name']; ?></span>
    </div>
<!--    <div class="mb-2">-->
<!--        <b>Photo:</b>-->
<!--        <img src="--><?//= $user['image']; ?><!--" alt="--><?//= $user['image']; ?><!--">-->
<!--    </div>-->
    <div class="mb-2">
        <b>Email:</b>
        <span><?= $user['email']; ?></span>
    </div>
    <div class="mb-2 text-center">
        <a href="user/edit" class="btn btn-primary">Edit profile</a>
        <a href="user/delete" class="btn btn-danger">Delete profile</a>
    </div>
</main>