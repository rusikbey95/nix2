<?php

namespace App\Controllers;

use BeiWork\Framework\Controllers\Controller;

/**
 * Class BaseController
 *
 * @author Ruslan Bei <rusikbey95@gmail.com>
 * @package App\Controllers
 */
class BaseController extends Controller
{
}