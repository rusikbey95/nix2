<?php

namespace App\Controllers;

use App\Models\UserModel;

/**
 * Class UserController
 *
 * @author Ruslan Bei <rusikbey95@gmail.com>
 * @package App\Controllers
 */
class UserController extends BaseController
{
    /**
     * @return bool
     */
    public function index(): bool
    {
        $title = 'Profile';
        return $this->view('user/index', ['title' => $title, 'user' => UserModel::getUser()]);
    }

    /**
     * @return bool
     */
    public function create(): bool
    {
        UserModel::created($_POST);
        return header('Location: ' . $_ENV['APP_URL'] . '/user');
    }

    /**
     * @return bool
     */
    public function edit(): bool
    {
        $title = 'Edit';
        return $this->view('user/edit', ['title' => $title, 'user' => UserModel::getUser()]);
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        UserModel::updated($_POST);
        return header('Location: ' . $_ENV['APP_URL'] . '/user');
    }

    /**
     * @return bool
     */
    public static function delete(): bool
    {
        UserModel::deleted($_POST);
        return header('Location: ' . $_ENV['APP_URL'] . '/');
    }

    /**
     * @return bool
     */
    public function register(): bool
    {
        $title = 'Registration';
        return $this->view('user/register', ['title' => $title, 'user' => UserModel::getUser()]);
    }

    /**
     * @return bool
     */
    public function login(): bool
    {
        $title = 'Login';
        return $this->view('user/login', ['title' => $title, 'user' => UserModel::getUser()]);
    }

    /**
     * @return bool
     */
    public function authorization(): bool
    {
        UserModel::authorized($_POST);
        return header('Location: ' . $_ENV['APP_URL'] . '/user');
    }

    /**
     * @return void
     */
    public static function logout(): void
    {
        session_destroy();
        header('Location: ' . $_ENV['APP_URL'] . '/');
    }
}