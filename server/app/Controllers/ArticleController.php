<?php

namespace App\Controllers;

use App\Models\ArticleModel;
use App\Models\UserModel;
use BeiWork\Framework\Models\Model;

/**
 * Class ArticleController
 *
 * @author Ruslan Bei <rusikbey95@gmail.com>
 * @package App\Controllers
 */
class ArticleController extends BaseController
{

    /**
     * @return array
     */
    public function currentUser(): array
    {
        if (UserModel::getUser() !== null){
            return UserModel::getUser();
        }
        return [];
    }

    /**
     * @return bool
     */
    public function list(): bool
    {
        $allUserArticles = ArticleModel::getAll();
        $user = ArticleController::currentUser();
        $title = 'Articles';
        return $this->view('article/list', compact('allUserArticles', 'user'));
    }

    /**
     * @return bool
     */
    public function page(): bool
    {
        $atricle = ArticleModel::getArticle();
        $user = ArticleController::currentUser();

        $title = 'Articles';
        return $this->view('article/list', compact('atricle', 'user'));
    }

    /**
     * @return bool
     */
    public function add(): bool
    {
        $title = 'Create';
        return $this->view('article/add', ['title' => $title, 'user' => ArticleController::currentUser()]);
    }

    /**
     * @return bool
     */
    public function create(): bool
    {
        ArticleModel::created($_POST);
        return header('Location: ' . $_ENV['APP_URL'] . '/list');
    }

    /**
     * @return bool
     */
    public static function delete(): bool
    {
        UserModel::deleted($_POST);
        return header('Location: ' . $_ENV['APP_URL'] . '/');
    }

    /**
     * @return bool
     */
    public function edit(): bool
    {
        $title = 'Edit';
        return $this->view('article/edit', ['title' => $title, 'user' => ArticleModel::getArticle()]);
    }
}