<?php

namespace App\Controllers;

use App\Models\UserModel;

/**
 * Class MainController
 *
 * @author Ruslan Bei <rusikbey95@gmail.com>
 * @package App\Controllers
 */
class MainController extends BaseController
{
    /**
     * @return bool
     */
    public function index(): bool
    {
        return $this->view('home/index', ['user' => UserModel::getUser()]);
    }
}