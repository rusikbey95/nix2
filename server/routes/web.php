<?php

use App\Controllers\ArticleController;
use App\Controllers\MainController;
use App\Controllers\UserController;
use BeiWork\Framework\Route;

Route::add('/',                   [MainController::class, 'index']);

Route::add('/login',              [UserController::class, 'login']);
Route::add('/logout',             [UserController::class, 'logout']);
Route::add('/register',           [UserController::class, 'register']);
Route::add('/user/authorization', [UserController::class, 'authorization']);


Route::add('/user',               [UserController::class, 'index']);
Route::add('/user/create',        [UserController::class, 'create']);
Route::add('/user/edit',          [UserController::class, 'edit']);
Route::add('/user/update',        [UserController::class, 'update']);
Route::add('/user/delete',        [UserController::class, 'delete']);




//Session
Route::add('/list',               [ArticleController::class, 'list']);
Route::add('/list/[^0-9]+',       [ArticleController::class, 'page']);
Route::add('/list/add',           [ArticleController::class, 'add']);
Route::add('/list/create',        [ArticleController::class, 'create']);
Route::add('/list/edit',          [ArticleController::class, 'edit']);
Route::add('/list/update',        [ArticleController::class, 'update']);